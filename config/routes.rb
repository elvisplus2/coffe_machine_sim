Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  get 'coffee_machine/show'
  post 'coffee_machine/command' => 'coffee_machine#command'

  root 'coffee_machine#show'

end
