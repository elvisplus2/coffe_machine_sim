const defaultTheme = require('tailwindcss/defaultTheme')


module.exports = {
  purge: {
    enabled: false,
    content: ['./app/**/*.html.*'],
  },
  darkMode: false, // or 'media' or 'class'
  variants: {
    extend: {
      opacity: ['disabled'],
    },
  },
  plugins: [
    // require('@tailwindcss/typography'),
    require('@tailwindcss/forms'),
    // require('@tailwindcss/line-clamp'),
    // require('@tailwindcss/aspect-ratio'),
  ],
}
