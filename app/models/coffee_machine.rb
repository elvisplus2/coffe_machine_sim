class CoffeeMachine
  include AASM

  COMMAND_CHANNEL_NAME = 'coffee_machine'
  # 30ml in 1 sec
  COFFE_RATE = 30

  DEFAULT_TEMP = 25
  DEFAULT_PRES = 0

  attr_accessor :temperature, :pressure, :coffee_size, :total_produced, :led, :error, :standby_timeout

  aasm whiny_transitions: false do
    state :power_off, initial: true, before_enter: proc { set_defaults }
    state :heating, before_enter: proc { self.led = :yellow }
    state :standby, before_enter: proc { (self.led = :green) && (self.standby_timeout = 10) }
    state :making_coffee, before_enter: proc { self.led = :yellow }
    state :pressurizing, before_enter: proc { self.led = :yellow }
    state :failed, before_enter: proc { self.led = :red }

    event :power_toggle do
      transitions from: :power_off, to: :heating
      transitions to: :power_off
    end

    event :heated do
      transitions from: :heating, to: :standby
    end

    event :fail do
      transitions to: :failed
    end

    event :pressurized do
      transitions from: :pressurizing, to: :making_coffee
    end

    event :cup_button_pushed do
      transitions from: :standby, to: :pressurizing
      transitions from: :pressurizing, to: :standby
      transitions from: :making_coffee, to: :standby
    end

    event :end_making_coffee do
      transitions from: :making_coffee, to: :standby
    end
  end

  def self.run
    @telemetry_buffer = []

    new.tap do |instance|
      instance.start do |telemetry|
        @telemetry_buffer << telemetry
        @telemetry_buffer.shift if @telemetry_buffer.length > 10

        Turbo::StreamsChannel.broadcast_update_to(
          'coffee_telemetry',
          target: 'telemetry',
          partial: 'coffee_machine/telemetry', locals: { '@telemetry': @telemetry_buffer }
        )

        Turbo::StreamsChannel.broadcast_update_to(
          'coffee_info',
          target: 'info',
          partial: 'coffee_machine/info', locals: { '@info': @telemetry_buffer.last }
        )
      end
    end
  end

  def initialize
    @redis = Redis.new(host: 'localhost', port: 6379)
    set_defaults
    self.total_produced = 0
  end

  def start(&callback)
    return if @started

    @callback = callback
    @last_telemetry_hash_sum = telemetry.hash

    Rails.logger.info("\u2615Starting...\u2615")
    @started = true

    # TODO: https://guides.rubyonrails.org/threading_and_code_execution.html#wrapping-application-code
    # TODO: close the thread gracefully
    Thread.new do
      @redis.subscribe(COMMAND_CHANNEL_NAME) do |on|
        on.message do |_channel, command|
          Rails.logger.info(['Received:', command])
          handle_command(command)
        end
      end
    end

    @task = Concurrent::TimerTask.new(execution_interval: 1) do |task|
      task.shutdown unless @started
      next_tick
    end
    @task.execute
    Rails.logger.info("\u2615Started\u2615")
  end

  def stop
    @started = false
    @redis.unsubscribe(COMMAND_CHANNEL_NAME)
  end

  def toggle_power_button
    power_toggle
  end

  def push_small_cup_button
    self.coffee_size = 5 * COFFE_RATE
    cup_button_pushed
  end

  def push_big_cup_button
    self.coffee_size = 10 * COFFE_RATE
    cup_button_pushed
  end

  def telemetry
    {
      state: aasm.current_state,
      led_state: led,
      error: error,
      heater_temperature_c: temperature.round(2),
      internal_pressure_barg: pressure.round(2),
      total_coffee_produced_ml: total_produced * COFFE_RATE
    }
  end

  private

  def set_defaults
    self.temperature = DEFAULT_TEMP
    self.pressure = DEFAULT_PRES
    self.led = :off
    self.error = false

    true
  end

  def handle_command(command)
    case command
    when 'toggle_power_button'
      toggle_power_button
    when 'push_small_cup_button'
      push_small_cup_button
    when 'push_big_cup_button'
      push_big_cup_button
    end
  rescue StandardError => e
    Rails.logger.error(e)
    self.error = e.message
    self.fail
  end

  def next_tick
    case aasm.current_state
    when :pressurizing
      PressurizeBehavior.next_tick(self)
    when :heating
      HeatBehavior.next_tick(self)
    when :making_coffee
      MakerBehavior.next_tick(self)
    when :standby
      StandbyBehavior.next_tick(self)
    end

    send_telemetry
  end

  def send_telemetry
    return unless @last_telemetry_hash_sum != telemetry.hash

    @last_telemetry_hash_sum = telemetry.hash
    @callback.call(telemetry.merge({ timestamp: Time.now }))
  end

  class HeatBehavior
    def self.next_tick(coffee_machine)
      if coffee_machine.temperature >= 58
        coffee_machine.heated
        return
      end
      coffee_machine.temperature += 4.2
    end
  end

  class PressurizeBehavior
    def self.next_tick(coffee_machine)
      if coffee_machine.pressure >= 12
        coffee_machine.pressurized
        return
      end
      coffee_machine.pressure += 3.5
    end
  end

  class StandbyBehavior
    def self.next_tick(coffee_machine)
      coffee_machine.standby_timeout -= 1
      coffee_machine.power_toggle if coffee_machine.standby_timeout <= 0
    end
  end

  class MakerBehavior
    def self.next_tick(coffee_machine)
      coffee_machine.end_making_coffee if coffee_machine.coffee_size <= 0
      coffee_machine.total_produced += 1
      coffee_machine.coffee_size -= 1 * COFFE_RATE
    end
  end
end
