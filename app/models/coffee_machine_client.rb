class CoffeeMachineClient
  def initialize
    @redis = Redis.new(host: 'localhost')
  end

  def toggle_power_button
    @redis.publish(CoffeeMachine::COMMAND_CHANNEL_NAME, 'toggle_power_button')
  end

  def push_small_cup_button
    @redis.publish(CoffeeMachine::COMMAND_CHANNEL_NAME, 'push_small_cup_button')
  end

  def push_big_cup_button
    @redis.publish(CoffeeMachine::COMMAND_CHANNEL_NAME, 'push_big_cup_button')
  end
end
