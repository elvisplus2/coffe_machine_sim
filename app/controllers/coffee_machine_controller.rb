class CoffeeMachineController < ApplicationController
  def show; end

  def command
    client = CoffeeMachineClient.new

    case params[:command]
    when 'toggle_power_button'
      client.toggle_power_button
    when 'push_small_cup_button'
      client.push_small_cup_button
    when 'push_big_cup_button'
      client.push_big_cup_button
    end
  end
end
